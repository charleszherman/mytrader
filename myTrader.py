
import csv
import sqlite3
import datetime
import os
import logging

class MyTrader():

#Process Trades and Update Positions Accordingly   


    def __init__ (self):
       
        
        # get db cursor and config logging 

        db_name = os.path.join('./data', 'trades.db')
        self.conn = sqlite3.connect(db_name)

        self.c = self.conn.cursor()

        #log_time = datetime.datetime.now()
        
        #log_name = 'trades_log_' + str(log_time) + '.log'

        #log_file = os.path.join('./report', log_name)

        log_file = self.log_config()

        logging.basicConfig(filename=log_file, filemode='w', format='%(name)s - %(levelname)s - %(message)s')
        #logging.warning('This will get logged to a file')
        #logging.error('%s raised an error', 'MyTrader')

    def log_config(self):
       
        # set log file name to current date + time

        log_time = datetime.datetime.now()
        
        log_name = 'trades_log_' + str(log_time) + '.log'

        log_file = os.path.join('./report', log_name)  

        return log_file 

    def load(self):

        # load trade data from csv file

         input_file = os.path.join('./data', 'trades.csv')

         with open(input_file) as f:
             #reader = csv.reader(f)
             reader = csv.DictReader(f) 
             for row in reader:
                 #print (row)
                 #print (row['trade_id'], row['version'], row['security'], row['qty'], row['dirction'],row['account'],row['operation'])
                 self.process_trade(row)

         #self.c.execute('SELECT * FROM Trades')
         #print(self.c.fetchall())

         #self.c.execute('SELECT * FROM Positions')
         #print(self.c.fetchall())      

    def create_db(self):

        #initialize tables for each run

        

        try:
            self.c.execute('''DROP TABLE Positions ''')
        except:
            print('Positions Table does not exist')

        try:
            self.c.execute('''DROP TABLE Trades ''')
        except:
            print('Trades Table does not exist')    

       
        self.c.execute('''CREATE TABLE Trades
            (trade_id text, version integer, security text, qty integer, direction text, account text, operation text, PRIMARY KEY (trade_id, version)) ''')  

        self.c.execute('''CREATE TABLE Positions
            (account text, security text, qty integer, trades text, PRIMARY KEY (account, security)) ''')     

    def create_report(self):

        # write position data out to csv file with current date + timestamp for each run

        report_time = datetime.datetime.now()
        #print (report_time)
        report_name = 'trade_report_' + str(report_time) + '.csv'
        #print (report_name)

        #file_name = os.path.join('/home/charlie/project/MyTrader/report', report_name)
        file_name = os.path.join('./report', report_name)
        #print(file_name)

        #positions = self.c.execute('SELECT * FROM Positions ORDER BY account, qty')
        positions = self.c.execute('SELECT * FROM Positions ORDER BY account, security DESC')

        if positions != None:
        
          with open(file_name, 'w') as f:

            writer = csv.writer(f)
            writer.writerows(positions)  

            #for row in positions:
             # f.write(row)

        return report_name     

            

    

    def process_trade(self,row):

        # main loop validate data and call appropriate method for operation type

        #print (row['trade_id'], row['version'], row['security'], row['qty'], row['direction'],row['account'],row['operation'])

        trade_id = row['trade_id']
        security = row['security']
        direction = row['direction']
        account = row['account']
        operation = row['operation']
        hold_qty = row['qty']
        hold_version = row['version']

        if hold_qty.isnumeric():
            qty = int(hold_qty)
        else:
            #print('Non numeric quanity')
            err_msg = trade_id + ' ' + hold_version + ' ' + security + ' ' + hold_qty + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - non numeric quanity ', err_msg)
            print('Non numeric quanity ' + err_msg)
            return    

        if hold_version.isnumeric():
            version = int(hold_version)
        else:
            #print('Non numeric version')
            err_msg = trade_id + ' ' + hold_version + ' ' + security + ' ' + hold_qty + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - non numeric version ', err_msg)
            print('Non numeric version ' + err_msg)
            return


        if version == 1 and operation != 'NEW':
            err_msg = trade_id + ' ' + hold_version + ' ' + security + ' ' + hold_qty + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - version 1 must be NEW ', err_msg)
            print('Version 1 must be NEW ' + err_msg)
            return

        if version > 1 and operation == 'NEW':
            err_msg = trade_id + ' ' + hold_version + ' ' + security + ' ' + hold_qty + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - version greater than 1 cannot be  NEW ', err_msg)
            print('Version greater than 1 cannot be NEW ' + err_msg)
            return    


        if version == 1 and operation == 'NEW':
            self.process_new(trade_id, version, security, qty, direction, account, operation)
        elif version > 1 and operation == 'AMMEND':  
            self.process_ammend(trade_id, version, security, qty, direction, account, operation)
        elif version > 1 and operation == 'CANCEL':
            self.process_cancel(trade_id, version, security, qty, direction, account, operation)
        else:
            #print ('Unknown operation')
            err_msg = trade_id + ' ' + str(version) + ' ' + security + ' ' + str(qty) + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - Unknown operation ', err_msg)
            print ('Unknown operation ' + err_msg)
            return             


 


    
    def process_new(self, trade_id, version, security, qty, direction, account, operation):

        #create new trades
        
        self.c.execute("INSERT INTO Trades VALUES (?,?,?,?,?,?,?)" , (trade_id, version, security, qty, direction, account, operation))
        
        trades = trade_id
        if direction == "SELL":
            qty *= -1

        self.c.execute("Select qty, trades from Positions WHERE account = ? and security = ?", (account,security))
        #print (self.c.fetchone())
        my_row = self.c.fetchone()
        if my_row == None:
            self.c.execute("INSERT INTO Positions VALUES (?,?,?,?)" , (account, security, qty, trades))
        else:
            my_qty = my_row[0]
            updated_qty = qty + my_qty
            updated_trades = my_row[1]
            if trade_id not in updated_trades:
                updated_trades = updated_trades + ', ' + trade_id
                     
                #updated_trades =  trade_id + ', ' + updated_trades

            self.c.execute("UPDATE Positions SET qty = ?, trades = ?  WHERE  account = ? and security = ?", (updated_qty, updated_trades, account, security)) 

    def process_ammend(self, trade_id, version, security, qty, direction, account, operation):

        #ammend existing trades

        #self.c.execute("INSERT INTO Trades VALUES (?,?,?,?,?,?,?)" , (trade_id, version, security, qty, direction, account, operation))
        self.c.execute("SELECT security, qty, direction, account, operation from Trades WHERE trade_id = ? and version = ? ", (trade_id, version -1 ))
        my_row = self.c.fetchone()
        if my_row == None:
            #print ("error - previous trade version not found, out of sequence")
            err_msg = trade_id + ' ' + str(version) + ' ' + security + ' ' + str(qty) + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - previous trade version not found, out of sequence', err_msg)
            print ("error - previous trade version not found, out of sequence " + err_msg)
            return

        else:
            prev_security = my_row[0]
            prev_qty = my_row[1]
            prev_direction = my_row[2]
            prev_account = my_row[3]
            prev_operation = my_row[4]

            #if prev_security != security:
            #    print ('error wrong security')
            #if prev_direction != direction:
            #    print ('error wrong direction')
            #if prev_account != account:
            #    print ('error wrong account')

        if prev_operation == 'CANCEL':
            #print  ('error already cancelled')
            err_msg = trade_id + ' ' + str(version) + ' ' + security + ' ' + str(qty) + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - already cancelled, out of sequence', err_msg)
            print  ('error already cancelled ' + err_msg)
            return 

        self.c.execute("INSERT INTO Trades VALUES (?,?,?,?,?,?,?)" , (trade_id, version, security, qty, direction, account, operation)) 

        #self.c.execute("Select qty, trades from Positions WHERE account = ? and security = ?", (account,security))
        self.c.execute("Select qty, trades from Positions WHERE account = ? and security = ?", (prev_account,prev_security))
        #print (self.c.fetchone())
        my_pos_row = self.c.fetchone()
        if my_pos_row == None:
            pass
            #print ("Error position not found")
        else:    
            pos_qty = my_pos_row[0]
            pos_trades = my_pos_row[1]

                     

        if prev_direction  == "BUY":
            pos_qty -= prev_qty
        else:
            pos_qty += prev_qty

        if prev_security == security and  prev_account == account:    

            if direction == "BUY":
                pos_qty += qty
            else:
                pos_qty -= qty 

                  
        self.c.execute("UPDATE Positions SET qty = ? WHERE  account = ? and security = ?", (pos_qty, prev_account, prev_security)) 

        if prev_security != security or  prev_account != account:

            if direction == "SELL":
                qty *= -1

            trades = trade_id    

            self.c.execute("Select qty, trades from Positions WHERE account = ? and security = ?", (account,security))

            my_row = self.c.fetchone()
            if my_row == None:
                self.c.execute("INSERT INTO Positions VALUES (?,?,?,?)" , (account, security, qty, trades))
            else:
                my_qty = my_row[0]
                updated_qty = qty + my_qty
                updated_trades = my_row[1]
                if trade_id not in updated_trades:
                    updated_trades = updated_trades + ' ' + trade_id
                    #updated_trades =  trade_id + ', ' + updated_trades
                self.c.execute("UPDATE Positions SET qty = ?, trades = ?  WHERE  account = ? and security = ?", (updated_qty, updated_trades, account, security))
        

    def process_cancel(self,trade_id, version, security, qty, direction, account, operation):

        #cancel existing trades

        #self.c.execute("INSERT INTO Trades VALUES (?,?,?,?,?,?,?)" , (trade_id, version, security, qty, direction, account, operation)) 
        self.c.execute("SELECT security, qty, direction, account, operation from Trades WHERE trade_id = ? and version = ? ", (trade_id, version -1 ))
        my_row = self.c.fetchone()
        if my_row == None:
            #print ("error - previous trade version not found, out of sequence")
            err_msg = trade_id + ' ' + str(version) + ' ' + security + ' ' + str(qty) + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - previous trade version not found, out of sequence', err_msg)
            print ("error - previous trade version not found, out of sequence " + err_msg)
            return
        else:
            prev_security = my_row[0]
            prev_qty = my_row[1]
            prev_direction = my_row[2]
            prev_account = my_row[3]
            prev_operation = my_row[4]

        if prev_security != security:
            print ('error wrong security')
            #if prev_direction != direction:
            #    print ('error wrong direction')
        if prev_account != account:
            print ('error wrong account')
            err_msg = trade_id + ' ' + str(version) + ' ' + security + ' ' + str(qty) + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - wrong account for cancel', err_msg)
            return

        if prev_operation == 'CANCEL':
            print  ('error already cancelled')
            err_msg = trade_id + ' ' + str(version) + ' ' + security + ' ' + str(qty) + ' '  + direction +  ' ' +  account + ' ' + operation
            logging.error('%s - already, out of sequence', err_msg)
            return  


        self.c.execute("INSERT INTO Trades VALUES (?,?,?,?,?,?,?)" , (trade_id, version, security, qty, direction, account, operation))

        self.c.execute("Select qty, trades from Positions WHERE account = ? and security = ?", (account,security))
        #print (self.c.fetchone())
        my_pos_row = self.c.fetchone()
        if my_pos_row == None:
            print ("Error position not found")
        else:    
            pos_qty = my_pos_row[0]
            pos_trades = my_pos_row[1]

        if prev_direction  == "BUY":
            pos_qty -= prev_qty
                         
        else:
            pos_qty += prev_qty
                         

        self.c.execute("UPDATE Positions SET qty = ? WHERE  account = ? and security = ?", (pos_qty, account, security))


    def get_postion_count(self):

        self.c.execute("SELECT COUNT(*) from Positions")
        my_pos_row = self.c.fetchone()
        if my_pos_row == None:
            count = 0
        else:
            count = my_pos_row[0]

        return count  

    def get_trade_count(self):

        self.c.execute("SELECT COUNT(*) from Trades")
        my_trade_row = self.c.fetchone()
        if my_trade_row == None:
            count = 0
        else:
            count = my_trade_row[0]

        return count

    def get_position(self, account, security):

        self.c.execute("SELECT  qty from Positions WHERE account = ? and security = ?", (account, security))

        my_pos_row = self.c.fetchone()
        if my_pos_row == None:
            qty = 0
        else:
            qty = my_pos_row[0]

        return qty    



   


def main():

    t = MyTrader()
    t.create_db()
    t.load()
    t.create_report()
    #t.get_postion_count()

if __name__ == "__main__":
    main()                 
             
