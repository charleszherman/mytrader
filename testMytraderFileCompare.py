import myTrader
import os



def main():

    t = myTrader.MyTrader()
    t.create_db()


    t.load()
    report_name = t.create_report()
    report_file_name = os.path.join('./report', report_name)

    gold_file_name = os.path.join('./report', 'gold.csv')

    

    with open(gold_file_name) as f1, open(report_file_name) as f2:
        for line1, line2 in zip(f1, f2):

             if line1 != line2:
                 print ("************* Different ****************")
                 print (line1)
                 print (line2)
                 print ("*****************************")




if __name__ == '__main__':
    main()