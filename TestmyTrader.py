import unittest
import myTrader
import filecmp
import os

class TestmyTrader(unittest.TestCase):

    
    
    def setUp(self):
                    
        self.t = myTrader.MyTrader()
        self.t.create_db()
        self.t.load()
        report_name = self.t.create_report()
        self.report_file_name = os.path.join('./report', report_name)
        #print (self.report_file_name)
        

        self.gold_file_name = os.path.join('./report', 'gold.csv')
        print (self.gold_file_name)
       

    #def tearDown(self):
    #    self.t.dispose()    

        
    
    def test_get_postion_count(self):
         self.assertEqual(self.t.get_postion_count(), (11), 'position count equals 11')

    def test_get_trade_count(self):
         self.assertEqual(self.t.get_trade_count(), (26), 'trade count equals 26')

    def test_get_position(self):
         self.assertEqual(self.t.get_position('ACC-1234', 'XYZ'), (150), 'position equals 150') 

    #def test_file_compare(self):
    #     self.assertTrue(filecmp.cmp(self.gold_file_name, self.gold_file_name, shallow=False))

    #def test_file_compare2(self):
    #     self.assertTrue(filecmp.cmp(self.report_file_name, self.report_file_name, shallow=False))

    #def test_file_compare3(self):
    #     self.assertTrue(filecmp.cmp(self.gold_file_name, self.report_file_name, shallow=False))                      



if __name__ == '__main__':
    unittest.main()