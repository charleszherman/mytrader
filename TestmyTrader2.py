import unittest
import myTrader
import filecmp
import os

class TestmyTrader(unittest.TestCase):

    
    
    def testMyTrader(self):

        t = myTrader.MyTrader()
        t.create_db()
        t.load()
        report_name = t.create_report()
        report_file_name = os.path.join('./report', report_name)
        
        gold_file_name = os.path.join('./report', 'gold.csv')

        self.assertEqual(t.get_postion_count(), (11), 'position count equals 11')

        self.assertEqual(t.get_trade_count(), (26), 'trade count equals 26')

        self.assertEqual(t.get_position('ACC-1234', 'XYZ'), (150), 'position equals 150')

        self.assertTrue(filecmp.cmp(gold_file_name, report_file_name, shallow=False))



        


if __name__ == '__main__':
    unittest.main()